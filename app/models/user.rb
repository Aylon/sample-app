class User < ApplicationRecord
    # before_save { email.downcase!  }
    # before_save { self.email = email.downcase  }
    before_save { self.email.downcase! unless self.email.blank? }
    # before_save { self.email = self.email.to_s.downcase }
    validates :name,  presence: true, length: { maximum: 50 }
    #VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    VALID_EMAIL_REGEX =  /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255 }, 
                            format: { with: VALID_EMAIL_REGEX},
                            uniqueness: { case_sensitive: false }
    VALID_PASSWORD_REGEX = /\A([a-zA-Z])*([0-9])*.{6,}\z/i                        
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 },
                            format: { with: VALID_PASSWORD_REGEX}
end
